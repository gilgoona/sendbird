//
//  SearchView.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/15.
//

import UIKit

class SearchView: UIViewController {
    
    let viewModel = SearchViewModel()
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    fileprivate lazy var searchResultTableView : SearchResultTableView  = {
        let searchResultTableView = SearchResultTableView()
        searchResultTableView.viewModel = self.viewModel
        return searchResultTableView
    }()
    
    fileprivate lazy var searchController: UISearchController = {
        var controller = UISearchController(searchResultsController: searchResultTableView)
        controller.searchResultsUpdater = searchResultTableView
        controller.searchBar.delegate = self
        controller.searchBar.placeholder = NSLocalizedString("Search", comment: "찾기")
        controller.dimsBackgroundDuringPresentation = true
        controller.hidesNavigationBarDuringPresentation = true
        controller.hidesBottomBarWhenPushed = true
        controller.isActive = true
        return controller
    }()
    
    override func viewDidLoad() {
                
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.largeTitleDisplayMode = .automatic
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo:self.view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo:self.view.leftAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo:self.view.widthAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        tableView.estimatedRowHeight = 44
        tableView.ut_registerClassCell(SearchKeywordCell.self)
        
        navigationItem.title = NSLocalizedString("Search", comment: "찾기")
        view.backgroundColor = .white
        searchResultTableView.delegate = self
        viewModel.delegate = self
        viewModel.localRecentlyKeywords()
    }
}

// MARK: - SearchResultTableViewDelegate
extension SearchView: SearchResultTableViewDelegate {
    func searchResultTableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(SearchResultSectionType(rawValue: indexPath.section)) {
        case .keyword:
            let keywords = self.viewModel.getKeywords()
            let data = keywords[indexPath.row]
            searchController.searchBar.text = data.keyword
        default:
            
            let detailbook = DetailBookView()
            let books = self.viewModel.getBooks()
            let data = books[indexPath.row]
            detailbook.viewModel.fetchBookDetail(isbn13: data.isbn13)
            self.navigationController?.pushViewController(detailbook, animated: true)
            if #available(iOS 11.0, *) { }
            else { searchController.isActive = false}
        }
    }
    
    func getRecentlyKeyword(_ keywords: [Keyword]) {
        tableView.reloadData()
    }
}

extension SearchView: SearchViewModelDelegate {
    func getKeywordsSuccess(data: [Keyword]) {
        tableView.reloadData()
    }
}

// MARK: - UISearchBarDelegate
extension SearchView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.isEmpty || text == "\n" {
            return true
        }
                
        return true
    }
}

// MARK : - UITableViewDelegate, UITableViewDataSource
extension SearchView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let keywords = viewModel.getRecentlyKeywords()
        return keywords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.ut_dequeueReusable(SearchKeywordCell.self, for: indexPath)
        let keywords = viewModel.getRecentlyKeywords()
        let data = keywords[indexPath.row]
        cell.title = data.keyword
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let keywords = self.viewModel.getRecentlyKeywords()
        let data = keywords[indexPath.row]
        searchController.searchBar.text = data.keyword
        searchController.isActive = true
    }
}
