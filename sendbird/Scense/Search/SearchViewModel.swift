//
//  SearchViewModel.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/15.
//

import Foundation

protocol SearchViewModelDelegate {
    func getKeywordsSuccess(data: [Keyword])
}

protocol SearchResultTableViewModelDelegate {
    func fetchSuccess(data: [Book])
    func fetchError(keyword:String, page: Int)
    func keywordAutoComplete(datas: [Keyword])
}

class SearchViewModel {
    public var delegate: SearchViewModelDelegate?
    public var resultDelegate: SearchResultTableViewModelDelegate?
    private var recentlyKeywords: [Keyword] = [Keyword]()
    private var keywords: [Keyword] = [Keyword]()
    private var books: [Book] = [Book]()
    private var isRequest = false
    private var currentPaging = 1
    private var currentKeyword = ""
    
    func getIsRequest() -> Bool {
        return isRequest
    }
        
    func getKeywords() -> [Keyword] {
        return keywords
    }
    
    func getBooks() -> [Book] {
        return books
    }
    
    func getCurrentKeyword() -> String {
        return currentKeyword
    }
    
    func getRecentlyKeywords() -> [Keyword] {
        return recentlyKeywords
    }
    
    func resetBooks() {
        self.books = [Book]()
        self.resultDelegate?.fetchSuccess(data: self.books)
    }
    
    func localRecentlyKeywords() {
        self.recentlyKeywords = DBHelper.shared.getRecentlyHistoryKeyword()
        self.delegate?.getKeywordsSuccess(data: self.recentlyKeywords)
    }
    
    func fetchBookList(searchText: String) {
        self.setRequestFlag(flag:true)
        let page = self.getCurrentPage(keyword: searchText)
        let addBooks = self.localBookList(keyword: searchText, page: page)
        if addBooks.count > 0 {
            self.setRequestFlag(flag:false)
            self.books.append(contentsOf: addBooks)
            self.resultDelegate?.fetchSuccess(data: self.books)
        } else {
            self.requestBookList(keyword: searchText, page: page)
        }
    }
    
    func getKeyword(searchText: String) {
        keywords = DBHelper.shared.getSearchHistoryKeyword(searchText)
        self.resultDelegate?.keywordAutoComplete(datas: keywords)
    }
    
    func requestBookList(keyword: String, page: Int) {
        let url = "\(API.Search)\(keyword)/\(page)"
        NetworkManager.shared.requestJSON(URL(string: url), httpMethod: .GET, parameters: [:], { (data) in
            self.setRequestFlag(flag:false)
            do {
                let model = try JSONDecoder().decode(SearchModel.self, from: data)
                if model.books.count > 0 {
                    _ = DBHelper.shared.upsertKeyword(keyword: keyword)
                    self.localRecentlyKeywords()
                    for insertData in model.books {
                        _ = DBHelper.shared.upsertSearchData(keyword: keyword, page: model.page, data: insertData)
                    }
                    let addBooks = DBHelper.shared.getBooks(keyword,page: page)
                    self.books.append(contentsOf: addBooks)
                    self.resultDelegate?.fetchSuccess(data: self.books)
                }
            } catch _ as NSError {
                self.resultDelegate?.fetchError(keyword: keyword, page: page)
            }
            
        }, { _ in
            self.setRequestFlag(flag:false)
            self.resultDelegate?.fetchError(keyword: keyword, page: page)
        })
    }
    
    private func localBookList(keyword: String, page: Int) -> [Book] {
        return DBHelper.shared.getBooks(keyword,page: page)
    }
    
    private func getCurrentPage(keyword: String) -> Int {
        if keyword != currentKeyword {
            currentKeyword = keyword
            self.resetBooks()
            currentPaging = 1
        } else {
            currentPaging += 1
        }
        
        return currentPaging
    }
    
    private func setRequestFlag(flag: Bool) {
        if isRequest == false {
            NetworkManager.shared.cancelCurrnetRequest()
        }
        isRequest = flag
    }
}
