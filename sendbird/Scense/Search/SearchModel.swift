//
//  SearchModel.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/15.
//

import Foundation

struct SearchModel: Codable {
    let error: String
    let total: String
    let page: String
    let books: [Book]
    
    private enum CodingKeys: String, CodingKey {
        case error
        case total
        case page
        case books
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.error = (try? values.decode(String.self, forKey: .error)) ?? ""
        self.total = (try? values.decode(String.self, forKey: .total)) ?? ""
        self.page = (try? values.decode(String.self, forKey: .page)) ?? ""
        self.books = (try? values.decode([Book].self, forKey: .books)) ?? [Book]()
    }
}

struct Book: Codable {
    let keyword: String
    let page: String
    let title: String
    let subtitle: String
    let isbn13: String
    let price: String
    let image: String
    let url: String
    let datetime: String
    
    private enum CodingKeys: String, CodingKey {
        case keyword
        case page
        case title
        case subtitle
        case isbn13
        case price
        case image
        case url
        case datetime
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.title = (try? values.decode(String.self, forKey: .title)) ?? ""
        self.subtitle = (try? values.decode(String.self, forKey: .subtitle)) ?? ""
        self.isbn13 = (try? values.decode(String.self, forKey: .isbn13)) ?? ""
        self.price = (try? values.decode(String.self, forKey: .price)) ?? ""
        self.image = (try? values.decode(String.self, forKey: .image)) ?? ""
        self.url = (try? values.decode(String.self, forKey: .url)) ?? ""
        self.keyword = ""
        self.page = ""
        self.datetime = ""
    }
    
    init(keyword:String, page:String, title:String, subtitle:String, isbn13:String, price:String, image:String, url:String, datetime:String) {
        self.keyword = keyword
        self.page = page
        self.title = title
        self.subtitle = subtitle
        self.isbn13 = isbn13
        self.price = price
        self.image = image
        self.url = url
        self.datetime = datetime
    }
}

struct Keyword {
    let keyword: String
    let insertDateTime: String
    
    init(keyword:String, insertDateTime:String) {
        self.keyword = keyword
        self.insertDateTime = insertDateTime
    }
}

