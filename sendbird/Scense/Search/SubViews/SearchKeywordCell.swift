//
//  SearchKeywordCell.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/17.
//

import UIKit

class SearchKeywordCell: UITableViewCell {

    fileprivate lazy var mainImageView : UIImageView  = {
        let mainImageView = UIImageView()
        return mainImageView
    }()
    
    fileprivate lazy var titleLabel : UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = UIFont(name: "System", size: 15)
        titleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        return titleLabel
    }()
    
    var title: String?  {
        didSet {
            titleLabel.text = title
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {

        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = .white
        
        self.contentView.addSubview(mainImageView)
        mainImageView.translatesAutoresizingMaskIntoConstraints = false
        mainImageView.topAnchor.constraint(equalTo:self.contentView.topAnchor, constant: 10).isActive = true
        mainImageView.bottomAnchor.constraint(equalTo:self.contentView.bottomAnchor, constant: -10).isActive = true
        mainImageView.leftAnchor.constraint(equalTo:self.contentView.leftAnchor, constant: 16).isActive = true
        mainImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        mainImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        mainImageView.image = UIImage(named: "Search")
        
        self.contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leftAnchor.constraint(equalTo:self.mainImageView.rightAnchor, constant: 10).isActive = true
        titleLabel.rightAnchor.constraint(equalTo:self.contentView.rightAnchor, constant: -10).isActive = true
        titleLabel.topAnchor.constraint(equalTo:self.contentView.topAnchor, constant: 10).isActive = true        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

