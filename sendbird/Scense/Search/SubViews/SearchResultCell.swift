//
//  SearchResultCell.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/15.
//

import UIKit

class SearchResultCell: UITableViewCell {
        
    var imageUrl: String? {
        didSet {
            let imgUrl = imageUrl ?? ""
            imageView?.setCacheImageURL(URL(string: imgUrl), { image in
                self.reloadInputViews()
            })
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
