//
//  SearchResultTableView.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/16.
//

import UIKit

enum SearchResultSectionType: Int {
    case keyword = 0
    case result
}

protocol SearchResultTableViewDelegate: NSObjectProtocol {
    func searchResultTableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    func getRecentlyKeyword(_ keywords: [Keyword])
}

class SearchResultTableView: UITableViewController {
        
    public var viewModel: SearchViewModel?
    public var delegate: SearchResultTableViewDelegate?
    
    // MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        
        tableView.ut_registerClassCell(SearchKeywordCell.self)
        tableView.ut_registerClassCell(SearchResultCell.self)
        tableView.estimatedRowHeight = UITableView.automaticDimension
        viewModel?.resultDelegate = self
        
        tableView.accessibilityIdentifier = "resultTableView"
    }
        
    // MARK: - TableView DataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(SearchResultSectionType(rawValue: section)) {
        case .keyword:
            return viewModel?.getKeywords().count ?? 0
        default:
            return viewModel?.getBooks().count ?? 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(SearchResultSectionType(rawValue: indexPath.section)) {
        case .keyword:
            let cell = tableView.ut_dequeueReusable(SearchKeywordCell.self, for: indexPath)
            let keywords = viewModel?.getKeywords()
            let data = keywords?[indexPath.row]
            cell.title = data?.keyword
            return cell
        default:
            let cell = tableView.ut_dequeueReusable(SearchResultCell.self, for: indexPath)
            let books = viewModel?.getBooks()
            let data = books?[indexPath.row]
            if let imageUrl = data?.image {
                if let url = URL(string: imageUrl) {
                    if let image = ImageDownLoadManager.shared.getCacheImage(url) {
                        cell.imageView?.image = image.imageResize(width: 64, height: 64)
                    } else {
                        cell.imageView?.image = UIImage(named: "placeholder")?.imageResize(width: 64, height: 64)
                        cell.imageUrl = data?.image
                    }
                }
            }
            
            cell.textLabel?.text = "\(data?.price ?? "") | \(data?.title ?? "")"
            cell.detailTextLabel?.text = data?.subtitle
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch(SearchResultSectionType(rawValue: indexPath.section)) {
        case .keyword:
            return 44
        default:
            return 82
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.searchResultTableView(tableView, didSelectRowAt: indexPath)
    }
    
    // MARK - UIScrollViewDelegate
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset < currentOffset {
            if viewModel?.getIsRequest() == false {
                viewModel?.fetchBookList(searchText: viewModel?.getCurrentKeyword() ?? "")
            }
        }
    }
}

// MARK: - UISearchResultsUpdating
extension SearchResultTableView: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        
        guard searchText.count > 0  else {
            viewModel?.resetBooks()
            return
        }
        
        viewModel?.fetchBookList(searchText: searchText)
        viewModel?.getKeyword(searchText: searchText)
    }
}

// MARK: - SearchViewModelDelegate
extension SearchResultTableView: SearchResultTableViewModelDelegate {
    
    func fetchSuccess(data: [Book]) {
        tableView.reloadData()
    }
    
    func keywordAutoComplete(datas: [Keyword]) {
        tableView.reloadData()
    }
    
    func fetchError(keyword:String, page: Int) {
        CommonAlerts.alertOkCancelHandler(title: "",
                                          message: NSLocalizedString("tryRequest", comment: "연결에 실패하였습니다.\n다시시도 하시겠습니까?"),
                                          okTitle: NSLocalizedString("retry", comment: "재시도"),
                                          cancelTitle: NSLocalizedString("cancel", comment: "취소"),
                                          completionHandler: {
                                            self.viewModel?.requestBookList(keyword: keyword,page:page)
                                          },
                                          cancelHandler: {})
    }
}
