//
//  DetailBookModel.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/15.
//

import Foundation

struct DetailBookModel: Codable {
    let error: String
    let title: String
    let subtitle: String
    let authors: String
    let publisher: String
    let language: String
    let isbn10: String
    let isbn13: String
    let pages: String
    let year: String
    let rating: String
    let desc: String
    let price: String
    let image: String
    let url: String
    var memo: String
    
    private enum CodingKeys: String, CodingKey {
        case error
        case title
        case subtitle
        case authors
        case publisher
        case language
        case isbn10
        case isbn13
        case pages
        case year
        case rating
        case desc
        case price
        case image
        case url
        case memo
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.error = (try? values.decode(String.self, forKey: .error)) ?? ""
        self.title = (try? values.decode(String.self, forKey: .title)) ?? ""
        self.subtitle = (try? values.decode(String.self, forKey: .subtitle)) ?? ""
        self.authors = (try? values.decode(String.self, forKey: .authors)) ?? ""
        self.publisher = (try? values.decode(String.self, forKey: .publisher)) ?? ""
        self.language = (try? values.decode(String.self, forKey: .language)) ?? ""
        self.isbn10 = (try? values.decode(String.self, forKey: .isbn10)) ?? ""
        self.isbn13 = (try? values.decode(String.self, forKey: .isbn13)) ?? ""
        self.pages = (try? values.decode(String.self, forKey: .pages)) ?? ""
        self.year = (try? values.decode(String.self, forKey: .year)) ?? ""
        self.rating = (try? values.decode(String.self, forKey: .rating)) ?? ""
        self.desc = (try? values.decode(String.self, forKey: .desc)) ?? ""
        self.price = (try? values.decode(String.self, forKey: .price)) ?? ""
        self.image = (try? values.decode(String.self, forKey: .image)) ?? ""
        self.url = (try? values.decode(String.self, forKey: .url)) ?? ""
        self.memo = ""
    }
    
    init(error:String, title: String, subtitle : String, authors : String, publisher : String, language : String, isbn10 : String, isbn13 : String, pages : String, year : String, rating : String, desc : String, price : String, image : String,
         url: String, memo: String) {
        self.error = error
        self.title = title
        self.subtitle = subtitle
        self.authors = authors
        self.publisher = publisher
        self.language = language
        self.isbn10 = isbn10
        self.isbn13 = isbn13
        self.pages = pages
        self.year = year
        self.rating = rating
        self.desc = desc
        self.price = price
        self.image = image
        self.url = url
        self.memo = memo
    }
}
