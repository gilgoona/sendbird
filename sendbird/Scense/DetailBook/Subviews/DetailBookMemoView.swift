//
//  DetailBookMemoView.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/20.
//

import UIKit

protocol DetailBookMemoViewDelegate {
    
}

class DetailBookMemoView: UIViewController  {
    public var viewModel: DetailBookViewModel?
    public var delegate: DetailBookMemoViewDelegate?
    
    fileprivate lazy var textView : UITextView  = {
        let textView = UITextView()
        textView.textAlignment = .justified
        textView.textColor = .black
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.isEditable = true
        textView.isScrollEnabled = true
        return textView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(textView)
        textView.delegate = self
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.topAnchor.constraint(equalTo:topLayoutGuide.bottomAnchor, constant:0).isActive = true
        textView.leftAnchor.constraint(equalTo:self.view.leftAnchor,constant:16).isActive = true
        textView.rightAnchor.constraint(equalTo:self.view.rightAnchor, constant:-16).isActive = true
        textView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor, constant:0).isActive = true
        textView.text = viewModel?.getDetailData()?.memo
        textView.becomeFirstResponder()
        
        navigationItem.title = NSLocalizedString("memo", comment: "메모")
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Close", comment: "닫기"),
                                                           style: .done,
                                                           target: self,
                                                           action: #selector(closeBtnAction))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Save", comment: "저장"),
                                                           style: .done,
                                                           target: self,
                                                           action: #selector(saveBtnAction))
        view.backgroundColor = .white
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let keyboardRect = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let frameKeyboard = keyboardRect.cgRectValue
        textView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: frameKeyboard.size.height,right: 0.0)
        view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        textView.contentInset = .zero
    }
    
    @objc func closeBtnAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveBtnAction() {
        viewModel?.updateMemo(text: textView.text)
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetailBookMemoView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
    if (textView.contentSize.height < 150){
        textView.isScrollEnabled = false
       }
       else{
         textView.isScrollEnabled = true
       }
      return true
    }
}
