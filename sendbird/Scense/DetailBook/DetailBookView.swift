//
//  DetailBookView.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/15.
//

import UIKit

class DetailBookView: UIViewController {
    
    let viewModel: DetailBookViewModel = DetailBookViewModel()
    
    fileprivate lazy var mainImageView : UIImageView  = {
        let mainImageView = UIImageView()
        mainImageView.contentMode = .scaleAspectFit
        return mainImageView
    }()

    fileprivate lazy var titleLabel : UILabel  = {
        let titleLabel = UILabel()
        titleLabel.font = UIFont(name: "System", size: 15)
        titleLabel.numberOfLines = 0
        titleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        return titleLabel
    }()
    
    fileprivate lazy var subtitleLabel : UILabel  = {
        let subtitleLabel = UILabel()
        subtitleLabel.font = UIFont(name: "System", size: 13)
        subtitleLabel.numberOfLines = 0
        subtitleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.33)
        return subtitleLabel
    }()
    
    fileprivate lazy var memoLabel : UILabel  = {
        let subtitleLabel = UILabel()
        subtitleLabel.font = UIFont(name: "System", size: 13)
        subtitleLabel.numberOfLines = 0
        subtitleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
        return subtitleLabel
    }()
    
    fileprivate lazy var memoBtn : UIButton = {
        let memoBtn = UIButton(type: .roundedRect)
        memoBtn.translatesAutoresizingMaskIntoConstraints = false
        memoBtn.setTitle(NSLocalizedString("MemoInput", comment: "메모입력"), for: .normal)
        memoBtn.addTarget(self, action: #selector(memoAction), for: .touchUpInside)
        memoBtn.borderCorners(1,UIColor(red: 0, green: 0, blue: 0, alpha: 0.33).cgColor ,15)
        return memoBtn
    }()
    
    fileprivate lazy var detailBtn : UIButton = {
        let detailBtn = UIButton(type: .roundedRect)
        detailBtn.translatesAutoresizingMaskIntoConstraints = false
        detailBtn.setTitle(NSLocalizedString("ViewDetails", comment: "상세보기"), for: .normal)
        detailBtn.addTarget(self, action: #selector(detailAction), for: .touchUpInside)
        detailBtn.borderCorners(1,UIColor(red: 0, green: 0, blue: 0, alpha: 0.33).cgColor ,15)
        return detailBtn
    }()
    
    fileprivate lazy var btnStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    @objc private func memoAction() {
        let detailbook = DetailBookMemoView()
        detailbook.viewModel = self.viewModel
        self.present(UINavigationController(rootViewController: detailbook), animated: true)
    }
    
    @objc private func detailAction() {
        let data = viewModel.getDetailData()
        if let url = URL(string: data?.url ?? "") {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.addSubview(mainImageView)
        mainImageView.translatesAutoresizingMaskIntoConstraints = false
        mainImageView.topAnchor.constraint(equalTo:topLayoutGuide.bottomAnchor, constant:0).isActive = true
        mainImageView.leftAnchor.constraint(equalTo:self.view.leftAnchor,constant:16).isActive = true
        mainImageView.rightAnchor.constraint(equalTo:self.view.rightAnchor, constant:-16).isActive = true
        
        view.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo:mainImageView.bottomAnchor, constant:10).isActive = true
        titleLabel.leftAnchor.constraint(equalTo:self.view.leftAnchor,constant:16).isActive = true
        titleLabel.rightAnchor.constraint(equalTo:self.view.rightAnchor, constant:-16).isActive = true
        
        view.addSubview(subtitleLabel)
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.topAnchor.constraint(equalTo:titleLabel.bottomAnchor, constant:10).isActive = true
        subtitleLabel.leftAnchor.constraint(equalTo:self.view.leftAnchor,constant:16).isActive = true
        subtitleLabel.rightAnchor.constraint(equalTo:self.view.rightAnchor, constant:-16).isActive = true
        
        view.addSubview(memoLabel)
        memoLabel.translatesAutoresizingMaskIntoConstraints = false
        memoLabel.topAnchor.constraint(equalTo:subtitleLabel.bottomAnchor, constant:10).isActive = true
        memoLabel.leftAnchor.constraint(equalTo:self.view.leftAnchor,constant:16).isActive = true
        memoLabel.rightAnchor.constraint(equalTo:self.view.rightAnchor, constant:-16).isActive = true
        
        view.addSubview(btnStackView)
        btnStackView.translatesAutoresizingMaskIntoConstraints = false
        btnStackView.topAnchor.constraint(equalTo:memoLabel.bottomAnchor, constant:10).isActive = true
        btnStackView.leftAnchor.constraint(equalTo:self.view.leftAnchor,constant:16).isActive = true
        btnStackView.rightAnchor.constraint(equalTo:self.view.rightAnchor, constant:-16).isActive = true
        btnStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btnStackView.addArrangedSubview(memoBtn)
        btnStackView.addArrangedSubview(detailBtn)
        
        view.backgroundColor = .white
        navigationItem.title = NSLocalizedString("DetailBook", comment: "Detail Book")
        viewModel.delegate = self
        
        self.updateView()
    }
    
    private func updateView(){
        let data = viewModel.getDetailData()
        if data?.image.count ?? 0 > 0 {
            if let url = URL(string: data?.image ?? "") {
                if let image = ImageDownLoadManager.shared.getCacheImage(url) {
                    mainImageView.image = image
                } else {
                    mainImageView.setCacheImageURL(url, { image in
                        self.mainImageView.image = image
                    })
                }
            }
        }
        
        titleLabel.text = data?.title
        subtitleLabel.text = data?.subtitle
        memoLabel.text = data?.memo
    }
}

// MARK : - DetailBookViewModelDelegate
extension DetailBookView: DetailBookViewModelDelegate {
    func fetchDetailBookSuccess(data: DetailBookModel?) {
        updateView()
    }
    
    func fetchDetailBookFail(isbn13: String){
        CommonAlerts.alertOkCancelHandler(title: "",
                                          message: NSLocalizedString("tryRequest", comment: "연결에 실패하였습니다.\n다시시도 하시겠습니까?"),
                                          okTitle: NSLocalizedString("retry", comment: "재시도"),
                                          cancelTitle: NSLocalizedString("cancel", comment: "취소"),
                                          completionHandler: { 
                                            self.viewModel.fetchBookDetail(isbn13: isbn13)
                                          },
                                          cancelHandler: {})
    }
    
    func updateMemoSuccess() {
        updateView()
    }
}
