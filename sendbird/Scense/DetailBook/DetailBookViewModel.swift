//
//  DetailBookViewModel.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/15.
//

import Foundation

protocol DetailBookViewModelDelegate: NSObjectProtocol {
    func fetchDetailBookSuccess(data: DetailBookModel?)
    func fetchDetailBookFail(isbn13: String)
    func updateMemoSuccess()
}

class DetailBookViewModel {
    public var delegate: DetailBookViewModelDelegate?
    private var data: DetailBookModel?
        
    func getDetailData() -> DetailBookModel? {
        return data
    }
    
    func updateMemo(text: String) {
        data?.memo = text
        _ = DBHelper.shared.upsertBookDetailData(detail: data)
        delegate?.updateMemoSuccess()
    }
    
    func fetchBookDetail(isbn13: String) {
        if let data = self.localBookData(isbn13: isbn13) {
            self.delegate?.fetchDetailBookSuccess(data: data)
        } else {
            self.requestDetailBook(isbn13: isbn13)
        }
    }
    
    private func localBookData(isbn13:String) -> DetailBookModel? {
        self.data = DBHelper.shared.getBookDetail(isbn13)
        return self.data
    }
    
    private func requestDetailBook(isbn13: String) {
        let url = "\(API.Book)\(isbn13)"
        NetworkManager.shared.requestJSON(URL(string: url), httpMethod: .GET, parameters: [:], { (data) in
            do {
                self.data = try JSONDecoder().decode(DetailBookModel.self, from: data)
                _ = DBHelper.shared.upsertBookDetailData(detail: self.data)
                self.delegate?.fetchDetailBookSuccess(data: self.data )
            } catch _ as NSError {
                self.delegate?.fetchDetailBookFail(isbn13: isbn13)
            }
            
        }, { _ in
            self.delegate?.fetchDetailBookFail(isbn13: isbn13)
        })
    }
}
