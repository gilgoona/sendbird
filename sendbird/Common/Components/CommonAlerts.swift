//
//  CommonAlerts.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/16.
//

import Foundation
import UIKit

class CommonAlerts {
    
    public static func alertOkCancelHandler(title: String?, message: String, okTitle: String, cancelTitle: String, completionHandler: @escaping () -> (), cancelHandler: @escaping () -> () ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: UIAlertAction.Style.default) { UIAlertAction in
            completionHandler()
        }
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) { UIAlertAction in
            cancelHandler()
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        if let topVC = UIApplication.topViewController() {  // 앨럿띄우기
            topVC.present(alertController, animated: true, completion: nil)
        }
    }
}
