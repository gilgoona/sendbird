//
//  DBHelper.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/16.
//

import Foundation
import SQLite3

public enum DatabaseError: Error {
    case openError
    case insertError
    case selectError
}

class DBHelper {
    
    static var shared:DBHelper = DBHelper()
    
    fileprivate let DB_VERSION = 1
    fileprivate var DB:OpaquePointer?

    func openDB() -> Bool{
        
        let fileURL = try! FileManager.default
            .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            .appendingPathComponent("search_\(DB_VERSION).sqlite")
        
        let res = sqlite3_open(fileURL.path, &DB)
        if res == SQLITE_OK {
            return true
        }
        
        return false
    }
    
    func closeDB(){
        sqlite3_close(DB);
    }
    
    func createDataBase() {
        createTableSearchKeyword()
        createTableSearchData()
        createTableBookDetail()
    }
    
    func createTableSearchKeyword() {
        if openDB() {
            var compiledStatement: OpaquePointer? = nil
            let query = """
            CREATE TABLE IF NOT EXISTS SEARCH_KEYWORD (
            keyword             TEXT        NOT NULL,
            datetime            DATE        NOT NULL,
            PRIMARY KEY (keyword));
            """
            
            let res = sqlite3_prepare_v2(DB, query, -1, &compiledStatement, nil);
            if res == SQLITE_OK {
                if sqlite3_step(compiledStatement) == SQLITE_DONE {
                    print("SEARCH_KEYWORD table created.")
                } else {
                    print("SEARCH_KEYWORD table could not be created.")
                }
            }
            let errmsg = String(cString: sqlite3_errmsg(DB)!)
            print(errmsg)
            sqlite3_finalize(compiledStatement);
            closeDB()
        }
    }
    
    func createTableSearchData() {
        if openDB() {
            var compiledStatement: OpaquePointer? = nil
            let query = """
            CREATE TABLE IF NOT EXISTS SEARCH_DATA (
            keyword             TEXT        NOT NULL,
            page                TEXT        NOT NULL,
            title               TEXT        NOT NULL,
            subtitle            TEXT        NOT NULL,
            isbn13              TEXT        NOT NULL,
            price               TEXT        NOT NULL,
            image               TEXT        NOT NULL,
            url                 TEXT        NOT NULL,
            datetime            DATE        NOT NULL,
            PRIMARY KEY (isbn13));
            """
            
            let res = sqlite3_prepare_v2(DB, query, -1, &compiledStatement, nil);
            if res == SQLITE_OK {
                if sqlite3_step(compiledStatement) == SQLITE_DONE {
                    print("SEARCH_DATA table created.")
                } else {
                    print("SEARCH_DATA table could not be created.")
                }
            }
            let errmsg = String(cString: sqlite3_errmsg(DB)!)
            print(errmsg)
            sqlite3_finalize(compiledStatement);
            closeDB()
        }
    }
    
    func createTableBookDetail() {
        if openDB() {
            var compiledStatement: OpaquePointer? = nil
            let query = """
            CREATE TABLE IF NOT EXISTS BOOK_DETAIL (
            title               TEXT        NOT NULL,
            subtitle            TEXT        NOT NULL,
            authors             TEXT        NOT NULL,
            publisher           TEXT        NOT NULL,
            language            TEXT        NOT NULL,
            isbn10              TEXT        NOT NULL,
            isbn13              TEXT        NOT NULL,
            pages               TEXT        NOT NULL,
            year                TEXT        NOT NULL,
            rating              TEXT        NOT NULL,
            desc                TEXT        NOT NULL,
            price               TEXT        NOT NULL,
            image               TEXT        NOT NULL,
            url                 TEXT        NOT NULL,
            memo                 TEXT        NOT NULL,
            PRIMARY KEY (isbn13));
            """
            
            let res = sqlite3_prepare_v2(DB, query, -1, &compiledStatement, nil);
            if res == SQLITE_OK {
                if sqlite3_step(compiledStatement) == SQLITE_DONE {
                    print("BOOK_DETAIL table created.")
                } else {
                    print("BOOK_DETAIL table could not be created.")
                }
            }
            let errmsg = String(cString: sqlite3_errmsg(DB)!)
            print(errmsg)
            sqlite3_finalize(compiledStatement);
            closeDB()
        }
    }
    
    func upsertKeyword(keyword:String) -> Int {
        if openDB() {
            let query = "INSERT OR REPLACE INTO SEARCH_KEYWORD (keyword, datetime) VALUES (?,?);"
            var compiledStatement: OpaquePointer? = nil
            let result = (sqlite3_prepare_v2(DB,query,-1, &compiledStatement, nil));
            if(result == SQLITE_OK) {
                let nskeyword: NSString = keyword as NSString
                let datetime: NSString = "\(Date())"  as NSString
                sqlite3_bind_text(compiledStatement, 1, nskeyword.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 2, datetime.utf8String, -1, nil)
                
                if sqlite3_step(compiledStatement) == SQLITE_DONE {
                    let id = sqlite3_last_insert_rowid(DB)
                    sqlite3_finalize(compiledStatement);
                    closeDB()
                    return Int(id)
                }
            }

            let errmsg = String(cString: sqlite3_errmsg(DB)!)
            print(errmsg)
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement);
            closeDB()
        }
        return -1
    }
    
    func upsertSearchData(keyword:String, page:String, data:Book)->Bool {
        if openDB() {
            let query = "INSERT OR REPLACE INTO SEARCH_DATA (keyword, page, title, subtitle, isbn13, price, url, image, datetime) VALUES (?,?,?,?,?,?,?,?,?);"
            var compiledStatement: OpaquePointer? = nil
            let result = (sqlite3_prepare_v2(DB,query,-1, &compiledStatement, nil));
            if(result == SQLITE_OK) {
                let nskeyword: NSString = keyword as NSString
                let nspage: NSString = page as NSString
                let title: NSString = data.title as NSString
                let subtitle: NSString = data.subtitle as NSString
                let isbn13: NSString = data.isbn13 as NSString
                let price: NSString = data.price as NSString
                let url: NSString = data.url as NSString
                let image: NSString = data.image as NSString
                let datetime: NSString = "\(Date())"  as NSString
                sqlite3_bind_text(compiledStatement, 1, nskeyword.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 2, nspage.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 3, title.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 4, subtitle.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 5, isbn13.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 6, price.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 7, url.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 8, image.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 9, datetime.utf8String, -1, nil)
                
                if sqlite3_step(compiledStatement) == SQLITE_DONE {
                    sqlite3_finalize(compiledStatement);
                    closeDB()
                    return true;
                }
            }

            let errmsg = String(cString: sqlite3_errmsg(DB)!)
            print(errmsg)
            sqlite3_finalize(compiledStatement);
            closeDB()
        }
        return false
    }
    
    func upsertBookDetailData(detail:DetailBookModel?) -> Int {
        if openDB() {
            let query = "INSERT OR REPLACE INTO BOOK_DETAIL (title, subtitle, authors, publisher, language, isbn10, isbn13, pages, year, rating, desc, price, image, url, memo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
            var compiledStatement: OpaquePointer? = nil
            let result = (sqlite3_prepare_v2(DB,query,-1, &compiledStatement, nil));
            if(result == SQLITE_OK) {
                let title: NSString = (detail?.title ?? "") as NSString
                let subtitle: NSString = (detail?.subtitle ?? "") as NSString
                let authors: NSString = (detail?.authors ?? "") as NSString
                let publisher: NSString = (detail?.publisher ?? "") as NSString
                let language: NSString = (detail?.language ?? "") as NSString
                let isbn10: NSString = (detail?.isbn10 ?? "") as NSString
                let isbn13: NSString = (detail?.isbn13 ?? "") as NSString
                let pages: NSString = (detail?.pages ?? "") as NSString
                let year: NSString = (detail?.year ?? "") as NSString
                let rating: NSString = (detail?.rating ?? "") as NSString
                let desc: NSString = (detail?.desc ?? "") as NSString
                let price: NSString = (detail?.price ?? "") as NSString
                let image: NSString = (detail?.image ?? "") as NSString
                let url: NSString = (detail?.url ?? "") as NSString
                let memo: NSString = (detail?.memo ?? "") as NSString
                
                sqlite3_bind_text(compiledStatement, 1, title.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 2, subtitle.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 3, authors.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 4, publisher.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 5, language.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 6, isbn10.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 7, isbn13.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 8, pages.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 9, year.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 10, rating.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 11, desc.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 12, price.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 13, image.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 14, url.utf8String, -1, nil)
                sqlite3_bind_text(compiledStatement, 15, memo.utf8String, -1, nil)
                
                if sqlite3_step(compiledStatement) == SQLITE_DONE {
                    let id = sqlite3_last_insert_rowid(DB)
                    sqlite3_finalize(compiledStatement);
                    closeDB()
                    return Int(id)
                }
            }

            let errmsg = String(cString: sqlite3_errmsg(DB)!)
            print(errmsg)
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement);
            closeDB()
        }
        return -1
    }
    
    func getBooks(_ keyword:String, page:Int) -> [Book] {
        var returnArray:[Book] = [Book]()
        if openDB() {
            let query = "SELECT * FROM SEARCH_DATA WHERE keyword = '\(keyword)' AND page = '\(page)'  ORDER BY datetime DESC;"
            var stmt: OpaquePointer? = nil
            if sqlite3_prepare_v2(DB, query, -1, &stmt, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(DB)!)
                print("error preparing insert:v1 \(errmsg)")
                sqlite3_finalize(stmt);
                closeDB()
                return returnArray
            }
            
            while(sqlite3_step(stmt) == SQLITE_ROW){
                let keywordText = String(cString: sqlite3_column_text(stmt, 0))
                let pageText = String(cString: sqlite3_column_text(stmt, 1))
                let title = String(cString: sqlite3_column_text(stmt, 2))
                let subtitle = String(cString: sqlite3_column_text(stmt, 3))
                let isbn13 = String(cString: sqlite3_column_text(stmt, 4))
                let price = String(cString: sqlite3_column_text(stmt, 5))
                let image = String(cString: sqlite3_column_text(stmt, 6))
                let url = String(cString: sqlite3_column_text(stmt,7))
                let datetime = String(cString: sqlite3_column_text(stmt, 8))
                let data = Book(keyword: keywordText, page: pageText, title: title, subtitle: subtitle, isbn13: isbn13, price: price, image: image, url: url, datetime: datetime)

                returnArray.append(data)
            }
            
            sqlite3_finalize(stmt);
            closeDB()
        }
        
        return returnArray
    }
        
    func getSearchHistoryKeyword(_ keyword: String) -> [Keyword] {
        var returnArray:[Keyword] = [Keyword]()
        if openDB() {
            let query = "SELECT * FROM SEARCH_KEYWORD WHERE keyword LIKE '\(keyword)%' Limit 3;"
            print("getSearchHistoryKeyword", query)
            var stmt: OpaquePointer? = nil
            if sqlite3_prepare_v2(DB, query, -1, &stmt, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(DB)!)
                print("error preparing insert:v1 \(errmsg)")
                sqlite3_finalize(stmt);
                closeDB()
                return returnArray
            }
            
            while(sqlite3_step(stmt) == SQLITE_ROW){
                let keyword = String(cString: sqlite3_column_text(stmt, 0))
                let insertDateTime = String(cString: sqlite3_column_text(stmt, 1))
                let data = Keyword(keyword: keyword, insertDateTime: insertDateTime)
                returnArray.append(data)
            }
            
            sqlite3_finalize(stmt);
            closeDB()
        }
        
        return returnArray
    }
    
    func getRecentlyHistoryKeyword() -> [Keyword] {
        var returnArray:[Keyword] = [Keyword]()
        if openDB() {
            let query = "SELECT * FROM SEARCH_KEYWORD ORDER BY datetime DESC;"
            var stmt: OpaquePointer? = nil
            if sqlite3_prepare_v2(DB, query, -1, &stmt, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(DB)!)
                print("error preparing insert:v1 \(errmsg)")
                sqlite3_finalize(stmt);
                closeDB()
                return returnArray
            }
            
            while(sqlite3_step(stmt) == SQLITE_ROW){
                let keyword = String(cString: sqlite3_column_text(stmt, 0))
                let insertDateTime = String(cString: sqlite3_column_text(stmt, 1))
                let data = Keyword(keyword: keyword, insertDateTime: insertDateTime)
                returnArray.append(data)
            }
            
            sqlite3_finalize(stmt);
            closeDB()
        }
        
        return returnArray
    }
    
    
    func getBookDetail(_ isbn13: String) -> DetailBookModel? {
        var detailData: DetailBookModel? = nil
        
        if openDB() {
            let query = "SELECT * FROM BOOK_DETAIL WHERE isbn13 = '\(isbn13)';"
            var stmt: OpaquePointer? = nil
            if sqlite3_prepare_v2(DB, query, -1, &stmt, nil) != SQLITE_OK{
                let errmsg = String(cString: sqlite3_errmsg(DB)!)
                print(errmsg)
                sqlite3_finalize(stmt);
                closeDB()
                return detailData
            }
            
            while(sqlite3_step(stmt) == SQLITE_ROW){
                let title = String(cString: sqlite3_column_text(stmt, 0))
                let subtitle = String(cString: sqlite3_column_text(stmt, 1))
                let authors = String(cString: sqlite3_column_text(stmt, 2))
                let publisher = String(cString: sqlite3_column_text(stmt, 3))
                let language = String(cString: sqlite3_column_text(stmt, 4))
                let isbn10 = String(cString: sqlite3_column_text(stmt, 5))
                let isbn13 = String(cString: sqlite3_column_text(stmt, 6))
                let pages = String(cString: sqlite3_column_text(stmt, 7))
                let year = String(cString: sqlite3_column_text(stmt, 8))
                let rating = String(cString: sqlite3_column_text(stmt, 9))
                let desc = String(cString: sqlite3_column_text(stmt, 10))
                let price = String(cString: sqlite3_column_text(stmt, 11))
                let image = String(cString: sqlite3_column_text(stmt, 12))
                let url = String(cString: sqlite3_column_text(stmt, 13))
                let memo = String(cString: sqlite3_column_text(stmt, 14))
                
                detailData = DetailBookModel(error:"", title: title, subtitle : subtitle, authors : authors, publisher : publisher, language : language, isbn10 : isbn10, isbn13 : isbn13, pages : pages, year : year, rating : rating, desc : desc, price : price, image : image, url: url, memo: memo)
            }
            
            sqlite3_finalize(stmt);
            closeDB()
        }
        
        return detailData
    }
}
