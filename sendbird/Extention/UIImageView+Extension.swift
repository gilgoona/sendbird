//
//  UIImageView+Extension.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/19.
//

import UIKit

extension UIImageView {
    func setCacheImageURL(_ url: URL?, _ success: ((_ image: UIImage) -> Void)? = nil) {
        let urlString = url?.absoluteString ?? ""
        if urlString.isEmpty {
            return
        }
        
        ImageDownLoadManager.shared.requestImageURL(url, { [weak self] (image) in
            self?.image = image
            if let success = success {
                success(image)
            }
        }) { (error) in
            
        }
    }
}
