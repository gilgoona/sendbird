//
//  String+Extension.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/20.
//

import Foundation

extension String {
    var insertText:String {
        let text = self.replacingOccurrences(of: "'", with: "\'")
        return text
    }
}
