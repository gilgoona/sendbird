//
//  UIView+Extension.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/17.
//

import Foundation
import UIKit

extension UIView {
    func borderCorners(_ borderWidth: CGFloat,_ borderColor:CGColor,_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
        
    }
}
