//
//  UIApplication+Extension.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/16.
//

import UIKit

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            let top = topViewController(nav.visibleViewController)
            return top
        }
        
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                let top = topViewController(selected)
                return top
            }
        }
        
        if let presented = base?.presentedViewController {
            let top = topViewController(presented)
            return top
        }
        
        if let alert = base as? UIAlertController {
            return alert.presentingViewController
        }
        
        if let alert = base?.popoverPresentationController, alert.delegate != nil{
            return alert.presentingViewController
        }
        
        return base
    }
}
