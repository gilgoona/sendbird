//
//  UICollectionView+Extension.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/20.
//

import UIKit

extension UICollectionView {

    func ut_registerClassCell<T>(_ cellType: T.Type) where T: UICollectionViewCell {
        let identifier = "\(cellType)"
        register(cellType, forCellWithReuseIdentifier: identifier)
    }

    func ut_dequeueReusable<T: UICollectionViewCell>(_ cell: T.Type, for indexPath: IndexPath) -> T {
        let cell = dequeueReusableCell(withReuseIdentifier: "\(T.self)", for: indexPath) as! T
        return cell
    }
}
