//
//  UIImage+Extension.swift
//  sendbird
//
//  Created by HyungGil Ham on 2021/06/19.
//

import UIKit

extension UIImage {
    func imageResize(width:CGFloat,height:CGFloat) -> UIImage {
        let newSize = CGSize(width: width, height: height)
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
