# My Bookshelf Application
### Requirements
##### [Search]
1. Shows book search information for specific keywords.
2. Allows keywords to be entered.
3. The design is not particularly limited.
4. Should show all the information (including images) from the response with JSON.  
5. Can use the interface builder or code implementation of the UI.
6. Caches the searched data.
Use the following API to request a search:
- Endpoint​: https://api.itbook.store/1.0/search/{query}
- Endpoint (with pagination)​: https://api.itbook.store/1.0/search/{query}/{page}
- Request method​: REST (GET)
- Response format​: JSON
- The response of the API request is page-separated. However, search results use
scrolling to eliminate page breaks. In other words, the search view should be able to show all the results smoothly using scrolling.
##### [Detail Book]
1. Shows detailed information of the selected book among the book list.
2. Allows the user to take a note.
3. Connects hyperlink with the book’s URL.
Use the following API to request detailed information.
- Endpoint​: https://api.itbook.store/1.0/books/{isbn13}
- Request method​: REST (GET)
- Response format​: JSON
Additional tasks for a project

##### All other specifications not mentioned affect the evaluation by considering it as an additional implementation.
- [ ] TDD (Test Driven Development)
- [X] Can use the interface builder or code implementation of the UI, but using the code
implementation of the UI will have a better effect on the evaluation.

### Check List
- [X] Don’t use any 3rd-party libraries.
- [X] The minimum deployment target is iOS 9.
- [X] Create an app that displays book information based on the UINavigationController and
UIViewControllers
- [X] In order to make the app more responsive, implement the image cache.
- [X] Handle the different situations of HTTP protocol that may happen.
- [X] The first view controller is ‘Search’.
- [X] Also, when one of the book lists is selected, there is a page showing the book detail
('Detail Book')
- [X] Implement in this order, “Search” > “Detail Book”. Even if it is not completed within the
deadline, it is recommended to submit only the parts you’ve completed
- [X] Used the "Music" app, Apple's official application, as a motif. If you do not understand the specifications described below during implementation, please refer to it or contact
our recruiter
- [X] This project refers to​ ​API book store​. (The API request can be failed and have some
bugs. We consider that point)
- [X] Share the project with us by version control system like Git(recommended) or by email
- [X] Regardless of function, please put the code you want to boast