//
//  SearchUITests.swift
//  sendbirdUITests
//
//  Created by HyungGil Ham on 2021/06/20.
//

import XCTest

class SearchUITests: XCTestCase {

    let app = XCUIApplication()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_searchBarResultTapToDetail() {
        let searchField = app.searchFields.firstMatch
        XCTAssertTrue(searchField.exists)
        
        searchField.tap()
        searchField.typeText("iOS")
        
        let resultCellOfFirst = app.cells.firstMatch
        XCTAssertTrue(resultCellOfFirst.waitForExistence(timeout: 10))
        resultCellOfFirst.tap()
    }
    
    func test_recentKeywordTapToDetail() {
        XCTAssertTrue(app.cells.firstMatch.exists)
        app.cells.firstMatch.tap()
        
        let resultCellOfFirst = app.cells.firstMatch
        XCTAssertTrue(resultCellOfFirst.waitForExistence(timeout: 10))
        resultCellOfFirst.tap()
    }
    
    func test_inputKeyboardMatchTapToDetail() {
        let searchField = app.searchFields.firstMatch
        XCTAssertTrue(searchField.exists)
        
        searchField.tap()
        searchField.typeText("ios")
        
        let tablesQuery = app.tables["resultTableView"]
        XCTAssert(tablesQuery.cells.count > 5)
        tablesQuery.cells.element(boundBy: 6).tap()
    }

}
